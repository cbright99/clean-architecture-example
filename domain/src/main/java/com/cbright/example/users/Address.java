package com.cbright.example.users;

public class Address {
    private String street;
    private String city;
    private String province;
    private String postalCode;

    /**
     Constructs a mailing address.
     @param street the street
     @param city the city
     @param province the province
     @param postalCode the ZIP postal code
     */
    public Address(String street, String city, String province, String postalCode) {
        this.street = street;
        this.city = city;
        this.province = province;
        this.postalCode = postalCode;
    }

    /**
     Formats the address.
     @return a string containing the address in mailing list format
     */
    public String format() {
        return street + "\n"
                + city + ", " + province + " " + postalCode;
    }

    public String getStreet() {
        return street;
    }
    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }
    public void setCity(String city) {
        this.city = city;
    }

    public String getProvince() {
        return province;
    }
    public void setProvince(String state) {
        this.province = state;
    }

    public String getPostalCode() {
        return postalCode;
    }
    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

}
