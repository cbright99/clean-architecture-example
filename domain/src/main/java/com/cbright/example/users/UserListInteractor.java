package com.cbright.example.users;

import java.util.List;

public class UserListInteractor {

    public interface onUserLoadedListener{
        void onSuccessfulUserListLoad(List<User> users);
        void onSuccessfulUserLoad(User article);
        void onError();
    }

    UserListInteractor.onUserLoadedListener listener;
    UserRepository userRepository;

    public UserListInteractor(UserListInteractor.onUserLoadedListener listener, UserRepository userRepository) {
        this.listener = listener;
        this.userRepository = userRepository;
    }

    public void getUser(long id) {
        List<User> users = userRepository.getUsers();
        for (User u : users) {
            if (u.getId() == id) {
                listener.onSuccessfulUserLoad(u);
                return;
            }
        }
    }

    public void getUserList() {
        List<User> users = userRepository.getUsers();
        listener.onSuccessfulUserListLoad(users);
    }

    public void addUser(User user) {
        userRepository.addUser(user);
    }

    public void getNewestUser() {
        List<User> users = userRepository.getUsers();
        if (users.size()>0) {
            User u = users.get(users.size() - 1);
            listener.onSuccessfulUserLoad(u);
        }
    }
}
