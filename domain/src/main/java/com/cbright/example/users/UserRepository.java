package com.cbright.example.users;

import java.util.List;

public interface UserRepository {
    void addUser(User user);
    List<User> getUsers();
}
