package com.cbright.example.users;

import java.util.List;

public class UserAddInteractor {
    public interface OnUserAddedListener {
        void onSuccessfulUserAdded(User article);
        void onError();
    }

    private final OnUserAddedListener listener;
    private final UserRepository userRepository;

    public UserAddInteractor(OnUserAddedListener listener, UserRepository userRepository) {
        this.listener = listener;
        this.userRepository = userRepository;
    }

    public void addUser(User user) {
        userRepository.addUser(user);
        getNewestUser();
    }

    private void getNewestUser() {
        final List<User> users = userRepository.getUsers();
        final User u = users.get(users.size()-1);
        listener.onSuccessfulUserAdded(u);
    }
}
