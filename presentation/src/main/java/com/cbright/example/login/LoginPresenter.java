package com.cbright.example.login;

public class LoginPresenter implements LoginContract.Presenter {

    LoginContract.View loginView;

    public LoginPresenter(LoginContract.View loginView) {
        this.loginView = loginView;
    }

    @Override
    public void onResume() {
        loginView.showOnboarding("Welcome to Magic List!");
    }

    @Override
    public void login(String password) {
        if (password.equals("test")){
            loginView.showConfirmMessage("Welcome!");
            loginView.showUserList(1);
        } else {
            loginView.showWrongPassword("Try Again!");
        }
    }
}