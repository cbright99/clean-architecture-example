package com.cbright.example.login;

public interface LoginContract {
    public interface Presenter {
        void onResume();
        void login(String password);
    }

    public interface View {
        void showOnboarding(String onboardMessage);
        void showWrongPassword(String errorMessage);
        void showConfirmMessage(String confirmMessage);
        void showUserList(long user);
    }
}
