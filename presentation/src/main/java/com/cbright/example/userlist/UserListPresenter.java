package com.cbright.example.userlist;

import com.cbright.example.users.UserListInteractor;
import com.cbright.example.users.UserRepository;;
import com.cbright.example.users.User;

import java.util.ArrayList;
import java.util.List;

public class UserListPresenter implements UserListContract.Presenter, UserListInteractor.onUserLoadedListener {

    UserListContract.View userListView;
    UserListInteractor userListInteractor;

    public UserListPresenter(UserListContract.View userListView, UserRepository userRepository) {
        this.userListView = userListView;
        userListInteractor = new UserListInteractor(this, userRepository);
    }

    @Override
    public void loadUser(long id) {
        userListInteractor.getUser(id);
    }

    @Override
    public void loadNewestUser() {
        userListInteractor.getNewestUser();
    }

    @Override
    public void loadUsers() {
        userListInteractor.getUserList();
    }

    @Override
    public void addUser(User user) {
        userListInteractor.addUser(user);
    }

    @Override
    public void onSuccessfulUserLoad(User user) {
        userListView.showUser(user);
    }

    @Override
    public void onSuccessfulUserListLoad(List<User> users) {
        List<UserListSummary> userListSummary = new ArrayList<>();
        for (User u : users) {
            userListSummary.add(
                new UserListSummary(u.getId(), chopString(u.getFirstName(),12), chopString(u.getLastName(),12))
            );
        }
        userListView.showUserList(userListSummary);
    }

    @Override
    public void onError() {
        userListView.showError();
    }

    private String chopString(String s, int max) {
        return s.substring(0,Math.min(s.length(), max));
    }
}