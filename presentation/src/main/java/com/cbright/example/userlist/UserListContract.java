package com.cbright.example.userlist;

import com.cbright.example.users.User;

import java.util.List;

public class UserListContract {
    public interface Presenter {
        void loadUser(long id);
        void addUser(User user);
        void loadNewestUser();
        void loadUsers();
    }

    public interface View {
        void showUserAdd();
        void showUser(User user);
        void showUserList(List<UserListSummary> userList);
        void showError();
    }
}
