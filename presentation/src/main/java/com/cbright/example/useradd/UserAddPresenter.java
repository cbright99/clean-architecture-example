package com.cbright.example.useradd;

import com.cbright.example.users.User;
import com.cbright.example.users.UserAddInteractor;
import com.cbright.example.users.UserRepository;

public class UserAddPresenter implements UserAddContract.Presenter, UserAddInteractor.OnUserAddedListener {
    private final UserAddContract.View userAddView;
    private final UserAddInteractor userAddInteractor;

    public UserAddPresenter(UserAddContract.View userAddView, UserRepository userRepository) {
        this.userAddView = userAddView;
        userAddInteractor = new UserAddInteractor(this, userRepository);
    }

    @Override
    public void addUser(User user) {
        userAddInteractor.addUser(user);
    }

    @Override
    public void onSuccessfulUserAdded(User user) {
        userAddView.notifyUser("onSuccessfulUserAdded()");
        userAddView.exitActivity(user);
    }

    @Override
    public void onError() {
        userAddView.showError();
    }
}