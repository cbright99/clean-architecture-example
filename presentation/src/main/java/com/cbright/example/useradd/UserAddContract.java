package com.cbright.example.useradd;

import com.cbright.example.users.User;

public class UserAddContract {
    public interface Presenter {
        void addUser(User user);
    }

    public interface View {
        void notifyUser(String notification);
        void exitActivity(User user);
        void showError();
    }
}
