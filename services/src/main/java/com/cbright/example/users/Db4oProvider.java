package com.cbright.example.users;

import android.content.Context;

public class Db4oProvider extends Db4oHelper {

    private static Db4oProvider provider = null;

    /**
     * Configure Db4oHelper by Passing Context.
     *
     * @param context
     */
    public Db4oProvider(Context context) {
        super(context);
    }

    public static Db4oProvider getInstance(Context ctx) {
        if (provider == null)
            provider = new Db4oProvider(ctx);
        return provider;
    }

    public void store(Object object) {
        db().store(object);
    }

    /*
    //This method is used to store the object into the database.
    public void store(User user) {
        db().store(user);
    }

    //This method is used to delete the object into the database.
    public void delete(User user) {
        db().delete(user);
    }

    //This method is used to retrive all object from database.
    public List<User> findAll() {
        return db().query(User.class);
    }

    //This method is used to retrive matched object from database.
    public List<User> getRecord(User user) {
        return db().queryByExample(user);
    }
    */

}

