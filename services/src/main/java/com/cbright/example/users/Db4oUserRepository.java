package com.cbright.example.users;

import android.content.Context;

import com.db4o.ObjectContainer;

import java.util.ArrayList;
import java.util.List;

public class Db4oUserRepository implements UserRepository {

    private static Db4oProvider provider;
    private static ObjectContainer db;

    public Db4oUserRepository(Context context) {
        this.loadDB(context);
    }

    private void loadDB(Context context) {
        // Setup for later use
        provider = new Db4oProvider(context);
        db = provider.db();

        // Uncomment and run app to clear out the db
        //deleteAllUsers();
    }

    @Override
    public void addUser(User user) {
        List<User> users = db.query(User.class);
        users = new ArrayList<>(users);
        user.setId(getHighestUserId()+1);
        users.add(user);
        provider.store(users);
        db.commit();
    }

    @Override
    public List<User> getUsers() {
        List<User> users2 = db.query(User.class);
        users2.toString();
        return users2;
    }

    // util to get the next index for adding
    private long getHighestUserId() {
        List<User> users = db.query(User.class);
        if (users.size()>=1) {
            User u = users.get(users.size() - 1);
            return u.getId();
        }
        else {
            return 0;
        }
    }

    // Method to clear out all the stored users
    private void deleteAllUsers() {
        List<User> users = db.query(User.class);
        for(User u: users) {
            db.delete(u);
        }
        db.commit();
    }
}

