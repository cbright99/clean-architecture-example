package com.cbright.example.users;

import java.io.IOException;

import android.content.Context;
import android.util.Log;

import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.config.EmbeddedConfiguration;

public class Db4oHelper {

    private static ObjectContainer oc = null;
    private Context context;

    /**
     * @param context
     */
    public Db4oHelper(Context context) {
        this.context = context;
    }

    /**
     * Create, open and close the database
     */
    public ObjectContainer db() {
        try {
            if (oc == null || oc.ext().isClosed()) {
                oc = Db4oEmbedded.openFile(Db4oEmbedded.newConfiguration(), db4oDBFullPath(context));
                oc.toString();
            }
        }
        catch (Exception ie) {
            Log.e(Db4oHelper.class.getName(), ie.toString());
        }

        return oc;
    }

    /**
     * Configure the behavior of the database
     */
    private EmbeddedConfiguration dbConfig() throws IOException {
        EmbeddedConfiguration configuration = Db4oEmbedded.newConfiguration();
        configuration.common().objectClass(User.class).objectField("firstName").indexed(true);
        configuration.common().objectClass(User.class).objectField("lastName").indexed(true);
        configuration.common().objectClass(User.class).cascadeOnUpdate(true);
        configuration.common().objectClass(User.class).cascadeOnActivate(true);
        return configuration;
    }

    /**
     * Returns the path for the database location
     */
    private String db4oDBFullPath(Context ctx) {
        return ctx.getDir("data", 0) + "/" + "customer.db4o";
    }

    /**
     * Closes the database
     */
    public void close() {
        if (oc != null) {
            oc.close();
        }
    }
}