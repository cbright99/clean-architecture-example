package com.cbright.example.user;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.Toast;

import com.cbright.example.users.User;
import com.cbright.example.app.R;
import com.cbright.example.useradd.UserAddContract;
import com.cbright.example.useradd.UserAddPresenter;
import com.cbright.example.users.Db4oUserRepository;
import com.cbright.example.utils.NameGenerator;
import com.cbright.example.utils.ToastUtil;

import java.security.SecureRandom;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.cbright.example.utils.ToastUtil.showShortToast;

public class UserAddActivity extends AppCompatActivity implements UserAddContract.View {

    @BindView(R.id.newFirstName)
    EditText firstName;
    @BindView(R.id.newLastName)
    EditText lastName;
    @BindView(R.id.newEmailAddress)
    EditText emailAddress;
    @BindView(R.id.newPhoneNumber)
    EditText phoneNumber;
    @BindView(R.id.newDateOfBirth)
    EditText dateOfBirth;

    private UserAddContract.Presenter presenter;

    private SecureRandom random = new SecureRandom();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_add);
        ButterKnife.bind(this);

        initActivity(savedInstanceState);
        presenter = new UserAddPresenter(this, new Db4oUserRepository(getApplicationContext()));

        // Put some data in the fields
        firstName.setText(NameGenerator.generateFirstName());
        lastName.setText(NameGenerator.generateLastName());

        // test - add random user - leave id at 0, will be incremented on insertation to db
        /*
        String firstName = NameGenerator.generateFirstName();
        String lastName = NameGenerator.generateLastName();
        User user = new User(0, firstName, lastName);
        presenter.addUser(user);
        */
    }

    private void initActivity(Bundle savedInstanceState) {
        // nothing here yet
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @OnClick(R.id.commit_new_user)
    public void submit(FloatingActionButton fab) {
        ToastUtil.showShortToast(UserAddActivity.this, "commit_new_user");
        User user = new User(0, firstName.getText().toString(), lastName.getText().toString());
        user.setEmailAddress(emailAddress.getText().toString());
        user.setPhoneNumber(phoneNumber.getText().toString());
        user.setDateOfBirth(dateOfBirth.getText().toString());
        presenter.addUser(user);
    }

    @Override
    public void exitActivity(User user) {
        ToastUtil.showShortToast(UserAddActivity.this,"New User = " + user.getFirstName() + " " + user.getLastName());
        Intent intent = new Intent(this, UserListActivity.class)
                .putExtra(UserListActivity.INTENT_EXTRA_USER_ID, user.getId());
        startActivity(intent);
    }

    @Override
    public void notifyUser(String notification) {
        Toast.makeText(getApplicationContext(), notification, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showError() {
        showShortToast(this, "Error adding user.");
    }
}
