package com.cbright.example.user;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.cbright.example.app.R;
import com.cbright.example.userlist.UserListSummary;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UserListArrayAdapter extends ArrayAdapter<UserListSummary> {

    public UserListArrayAdapter(Context context, List<UserListSummary> objects) {
        super(context, 0, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        UserListSummary userSummary = getItem(position);
        UserListArrayAdapter.ViewHolder viewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.user, parent, false);
            viewHolder = new UserListArrayAdapter.ViewHolder(convertView);
            convertView.setTag(viewHolder);
        }
        viewHolder = (UserListArrayAdapter.ViewHolder) convertView.getTag();

        setViews(userSummary, viewHolder);

        return convertView;
    }

    private void setViews(UserListSummary userListSummary, UserListArrayAdapter.ViewHolder viewHolder) {
        viewHolder.userIdTV.setText(""+ userListSummary.getId());
        viewHolder.firstNameTV.setText(userListSummary.getFirstName());
        viewHolder.lastNameTV.setText(userListSummary.getLastName());
    }

    static class ViewHolder {
        @BindView(R.id.userIdTV)
        TextView userIdTV;
        @BindView(R.id.firstNameTV)
        TextView firstNameTV;
        @BindView(R.id.lastNameTV)
        TextView lastNameTV;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
