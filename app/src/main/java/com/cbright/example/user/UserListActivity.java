package com.cbright.example.user;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;
import android.widget.TextView;

import com.cbright.example.userlist.UserListPresenter;
import com.cbright.example.users.Db4oUserRepository;
import com.cbright.example.users.User;
import com.cbright.example.app.R;
import com.cbright.example.userlist.UserListSummary;
import com.cbright.example.userlist.UserListContract;
import com.cbright.example.utils.ToastUtil;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;

import static com.cbright.example.utils.ToastUtil.showShortToast;

public class UserListActivity extends AppCompatActivity implements UserListContract.View {

    public static final String INTENT_EXTRA_USER_ID = "INTENT_EXTRA_USER_ID";
    public static final String INSTANCE_STATE_USER_ID = "INSTANCE_STATE_USER_ID";

    @BindView(R.id.newestUser)
    TextView newestUser;
    @BindView(R.id.firstName)
    TextView firstName;
    @BindView(R.id.lastName)
    TextView lastName;
    @BindView(R.id.emailAddress)
    TextView emailAddress;
    @BindView(R.id.phoneNumber)
    TextView phoneNumber;
    @BindView(R.id.dateOfBirth)
    TextView dateOfBirth;

    @BindView(R.id.user_list)
    ListView userList;

    private long userId;
    UserListArrayAdapter UserListAdapter;
    private UserListContract.Presenter presenter;

    private SecureRandom random = new SecureRandom();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_list);
        ButterKnife.bind(this);

        initActivity(savedInstanceState);
        presenter = new UserListPresenter(this, new Db4oUserRepository(getApplicationContext()));

        // just return the latest user and then displays it
        presenter.loadNewestUser();

        // set up adapter for the list of users
        UserListAdapter = new UserListArrayAdapter(this, new ArrayList<UserListSummary>());
        userList.setAdapter(UserListAdapter);

        // load all the users
        presenter.loadUsers();
    }

    @OnClick(R.id.add_new_user)
    public void submit(FloatingActionButton fab) {
        ToastUtil.showShortToast(UserListActivity.this, "onAddSelected()");
        showUserAdd();
    }

    @Override
    public void showUserAdd() {
        Intent intent = new Intent(this, UserAddActivity.class);
        startActivity(intent);
    }

    @OnItemClick(R.id.user_list)
    void onArticleSelected(int position) {
        UserListSummary userListSummary = UserListAdapter.getItem(position);
        ToastUtil.showShortToast(UserListActivity.this,"User = " + userListSummary.getFirstName() + " " + userListSummary.getLastName());
        newestUser.setText("Selected User :");
        presenter.loadUser(userListSummary.getId());
    }

    private void initActivity(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            this.userId = getIntent().getLongExtra(INTENT_EXTRA_USER_ID, 0L);
        } else {
            this.userId = savedInstanceState.getInt(INSTANCE_STATE_USER_ID);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (outState != null) {
            outState.putLong(INSTANCE_STATE_USER_ID, this.userId);
        }
        super.onSaveInstanceState(outState);
    }

    @Override
    public void showUser(User user) {
        firstName.setText(user.getFirstName());
        lastName.setText(user.getLastName());
        emailAddress.setText(user.getEmailAddress());
        phoneNumber.setText(user.getPhoneNumber());
        dateOfBirth.setText(user.getDateOfBirth());
    }

    @Override
    public void showUserList(List<UserListSummary> userList) {
        UserListAdapter.addAll(userList);
    }

    @Override
    public void showError() {
        showShortToast(this, "Error loading article.");
    }
}
